const express = require ("express");
const app = express();
const PORT = 3000;

// build small REST API with Express

console.log("Server-side program starting...");

// Baseurl: http://localhost:3000
// Endpoint: http://localhost:3000/
app.get('/', (req, res) => {
    // req = request, res = response
    res.send("Hello world");
});

/**
 * This is arrow function to add two numbers together.
 * @param {Number} a is the first param
 * @param {Number} b is the second param
 * @return {Number} 
 */
const add = (a, b) => {
    const sum = a + b;
    return sum;
};

// Adding endpoint: http://localhost:3000/add?a=value&b=value
app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum.toString());
})

app.listen(PORT, () => console.log(
    `Server listening at http://localhost:${PORT}`
));